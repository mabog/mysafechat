import {sequelize} from '../sequelize.mjs';
import DataTypes from 'Sequelize';

const RoomRepository = sequelize.define('Room', {
    // id: {type: DataTypes.STRING, primaryKey:true}
    name: {type: DataTypes.STRING},
    link: {type: DataTypes.STRING, allowNull: true},
}, {});

export {RoomRepository};
