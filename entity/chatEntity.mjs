import typeorm from 'typeorm';
const {EntitySchema} = typeorm;
import {Chat} from '../model/Chat.mjs'
import {Room} from '../model/Room.mjs'

const ChatEntity = new EntitySchema({
    name: "Chat",
    target: Chat,
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        Room: {
            type: "varchar"
        },

    },
    // relations: {
    //     rooms: {
    //         target: "Room",
    //         type: "one-to-many",
    //         joinTable: true,
    //         cascade: true
    //     }
    // }
    })

    export {ChatEntity}