class Room {
    constructor(name, messages) {
        this.name = name;
        this.link = this.generateLink();
        this.messages = messages;
        // this.host = host;
        // this.invited = invited;
    }

    generateLink() {
        return Math.random().toString(36).substring(2);
    }

    saveMsg(text) {
        this.messages.push(text);
    }

}

export {Room};
    