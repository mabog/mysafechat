import { MessageRepository } from "../entity/MessageRepository.mjs"
import { RoomRepository } from "../entity/RoomRepository.mjs";

class Chat {
    constructor() {}

    async getaRoom(link) {
        try{
            const room1 = await RoomRepository.findOne({
                where: {link: `${link}`},
                include: MessageRepository
            });
            return room1.toJSON();
        }catch(err){
            console.log('getaRoom: ', err)
        }
    }

    async getAllRooms() {
        try{
            return RoomRepository.findAll();
        }catch(err){
            console.log('getAllRooms: ', err)
        }
    }

    async saveRoom(room) {
        try{
            return RoomRepository.create(room);
        }catch(err){
            console.log('createRoom: ', err)
        }
    }

    async saveMsg(msg) {
        try{
            return MessageRepository.create(msg);
        }catch(err){
            console.log('createRoom: ', err)
        }
    }
    

};

export {Chat}