import "reflect-metadata";
import express from 'express';
import {Room} from './model/Room.mjs';
import {Message} from './model/Message.mjs'
import {Chat} from './model/Chat.mjs';
import { MessageRepository } from "./entity/MessageRepository.mjs";
import { RoomRepository } from "./entity/RoomRepository.mjs";

const app = express();
const port = 3000
app.use(express.urlencoded({ extended: true }));

// Get rooms
app.get('/', async (_req, res) => {
  res.send(await myChat.getAllRooms());
});

//Get a room
app.get('/:roomLink', async (req, res) => {
  res.send(await myChat.getaRoom(req.params.roomLink));
});

// Create room
app.post('/', async (req, res) => {
  const newRoom = new Room(req.body.name);
  res.json(await myChat.saveRoom(newRoom));
});

// Create message
app.post('/:roomLink', async (req, res) => {
  const room = await myChat.getaRoom(req.params.roomLink);
  const msg = new Message(req.body.text, room.id);
  res.json(await myChat.saveMsg(msg));
});

// Run server
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});

const myChat = new Chat();

const sequelizeConn = async () => {

  try{
    RoomRepository.hasMany(MessageRepository);
    MessageRepository.belongsTo(RoomRepository);

    MessageRepository.sync({ alter: true });
    RoomRepository.sync({ alter: true });
  }catch(err){
    console.log('sequelizeConn:', err)
  }

}

sequelizeConn();
