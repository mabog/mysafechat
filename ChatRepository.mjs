import Sequelize from 'Sequelize';
import DataTypes from 'Sequelize';
import { ChatEntity } from "./entity/chatEntity.mjs"
import { RoomRepository } from "./entity/RoomRepository.mjs"
import { MessageRepository } from "./entity/MessageRepository.mjs"
import {Room} from './model/Room.mjs'
import {Message} from './model/Message.mjs'
import {Chat} from './model/Chat.mjs'

class ChatRepository {

    constructor() {
        const sequelize = new Sequelize('safechat', 'root', 'zemqta2', {
            host: 'localhost',
            dialect: 'mysql' 
        });
        this.sequelize = sequelize;
        
    };

    saveMsg(msg) {
        MessageRepository.create(msg);
    }
}
export {ChatRepository}